function fadeInSlide() {
	$(".apps .acontent").show("slide", {
		duration: 200,
		direction: 'left'
	}).show();
	//$(".apps .content").fadeIn(1250).dequeue(); // Macht zu viel Stress
}


function fadeinSlideFrame() {
	$(".apps .acontent").show("slide", {
		duration: 200,
		direction: 'left'
	}).hide();
	$("iframe").show("slide", {
		duration: 200,
		direction: 'right'
	}).show();	
}

$(document).ready(function () {
	fadeInSlide();
	setCalenderApp();
	$('td #appicon').click(function () {
		resourceCall('switchApp', $(this).attr('name'));
	});
	$('td .appicon').click(function() {

	});
	setFirstApp();

});

var weekdays = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];

function setCalenderApp() {
	var d = new Date();
	$('#calimg').attr('src','../../os/ios/images/weekdays/' + weekdays[d.getDay()] + '.png');
	$('span#day').html(d.getDate());
}

var start;
function setFirstApp() {
	$('.appicon:first p img').addClass('appicon active');
	start = document.getElementById('active');
	//start.childNodes.length;
}

document.onkeydown = checkKey;


function dotheneedful(sibling) {
	if (sibling != null) {
		start.focus();
		$('#active .appicon p img').removeClass();
		start.id = "";
		sibling.focus();
		start = sibling;
		sibling.id = 'active';
		$('#active .appicon p img').addClass('appicon active');
	}
}

function checkKey(e) {
	e = e || window.event;
	if(e.keyCode == '13') {
		console.log($('#active .appicon').attr('name'));
		$('#myframe').attr('src','../../os/ios/' + $('#active .appicon').attr('name') + '.html');
		fadeinSlideFrame();
	}
	else if (e.keyCode == '38') {
		// up arrow
		var idx = start.cellIndex;
		var nextrow = start.parentElement.previousElementSibling;
		if (nextrow != null) {
			var sibling = nextrow.cells[idx];
			dotheneedful(sibling);
		}
	} else if (e.keyCode == '40') {
		// down arrow
		var idx = start.cellIndex;
		var nextrow = start.parentElement.nextElementSibling;
		if (nextrow != null) {
			var sibling = nextrow.cells[idx];
			dotheneedful(sibling);
		}
	} else if (e.keyCode == '37') {
		// left arrow
		var sibling = start.previousElementSibling;
		dotheneedful(sibling);
	} else if (e.keyCode == '39') {
		// right arrow
		var sibling = start.nextElementSibling;
		dotheneedful(sibling);
	}
}
