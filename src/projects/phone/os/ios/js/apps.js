function fadeInSlide() {
	$(".apps .content").show("slide", {
		duration: 200,
		direction: 'left'
	}).show();
	//$(".apps .content").fadeIn(1250).dequeue(); // Macht zu viel Stress
}


$(document).ready(function () {
	fadeInSlide();
	setCalenderApp();
	$('td #appicon').click(function () {
		resourceCall('switchApp', $(this).attr('name'));
	});
	$('td .appicon').click(function() {
		console.log("TEST");
		parent.parentFunc("TEST");
	});

});

var weekdays = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];

function setCalenderApp() {
	var d = new Date();
	$('#calimg').attr('src','images/weekdays/' + weekdays[d.getDay()] + '.png');
	$('span#day').html(d.getDate());
}
