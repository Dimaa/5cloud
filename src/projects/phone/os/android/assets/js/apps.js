function fadeInSlide() {
  $(".apps .content").show('slide', {
    duration: 650,
    direction: 'left'
  }).hide();
  $('.apps .content').fadeIn(1000).dequeue();
}

function changeCalender() {
  var date = new Date();
  var day = date.getDate();
  
  $('#appicon[name="calender"] img').attr('src', 'assets/images/icons/apps/calender/calendar2_' + day + '.png')
}

$(document).ready(function () {
  fadeInSlide();
  changeCalender();

  $('td #appicon').click(function () {
    //resourceCall('switchApp', $(this).attr('name'));
  });
});
