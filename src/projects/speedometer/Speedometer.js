"use strict";
/// <reference path="../../types-gt-mp/index.d.ts" />
var player = API.getLocalPlayer();
var browser = null;
var lastInVehicle = false;
API.onResourceStart.connect(function () {
    createSpeedometer();
});
API.onResourceStop.connect(function () {
    destroySpeedometer();
});
API.onUpdate.connect(function () {
    if (browser !== null) {
        var inVehicle = API.isPlayerInAnyVehicle(player);
        if (inVehicle) {
            var vehicle = API.getPlayerVehicle(player);
            var velocity = API.getEntityVelocity(vehicle);
            var speed = Math.sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y + velocity.Z * velocity.Z) * 3.6;
            browser.call('setCurrentSpeed', speed);
            if (!lastInVehicle) {
                var maxSpeed = API.getVehicleMaxSpeed(API.getEntityModel(vehicle)) * 3.6;
                browser.call('setMaxSpeed', maxSpeed * 1.35);
                showSpeedometer();
            }
        }
        else if (lastInVehicle) {
            hideSpeedometer();
        }
        lastInVehicle = inVehicle;
    }
});
API.onServerEventTrigger.connect(function (eventName, args) {
    switch (eventName) {
        case 'changeFuel':
            var value = args[0];
            browser.call('setCurrentFuel', value);
            break;
    }
});
function createSpeedometer() {
    var resolution = API.getScreenResolution();
    var speedoSize = resolution.Height / 4;
    browser = API.createCefBrowser(speedoSize, speedoSize);
    API.setCefBrowserPosition(browser, resolution.Width - speedoSize, resolution.Height - speedoSize);
    API.setCefBrowserHeadless(browser, true);
    API.waitUntilCefBrowserInit(browser);
    API.loadPageCefBrowser(browser, 'projects/speedometer/CEF/index.html');
}
function destroySpeedometer() {
    if (browser !== null) {
        API.destroyCefBrowser(browser);
    }
}
function showSpeedometer() {
    if (browser !== null) {
        API.setCefBrowserHeadless(browser, false);
    }
    else {
        createSpeedometer();
        showSpeedometer();
    }
}
function hideSpeedometer() {
    if (browser !== null) {
        API.setCefBrowserHeadless(browser, true);
    }
}
