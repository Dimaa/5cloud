var Speedometer = function (userPref, object) {
  var self = this;
  this.defaultProperty = {
    /* Max value of the meter */
    maxSpeed: 240,
    /* Division value of the meter */
    divFact: 10,
    /* more than this level, color will be red */
    dangerLevel: 200,
    /* reading begins angle */
    initDeg: -45,
    /* total angle of the meter reading */
    maxDeg: 270,
    /* radius of the meter circle */
    edgeRadius: 104,
    /* speed nobe height */
    speedNobeH: 4,
    /* speed nobe width */
    speedoNobeW: 0,
    /* speed nobe left position */
    speedoNobeL: -3,
    /* radius of indicators position */
    indicatorRadius: 125,
    /* radius of numbers position */
    indicatorNumbRadius: 85,
    /* speedo-meter current value cont */
    speedPositionTxtWH: 80,
    /* indicator number width */
    numbW: 30,
    /* indicator number height */
    numbH: 16,
    /* no of small div between main div */
    noOfSmallDiv: 2,
  }

  if (typeof userPref === 'object')
    for (var prop in userPref)
      this.defaultProperty[prop] = userPref[prop];

  var speedInDeg,
    noOfDev = this.defaultProperty.maxSpeed / this.defaultProperty.divFact,
    divDeg = this.defaultProperty.maxDeg / noOfDev,
    speedBgPosY,
    speedoWH = this.defaultProperty.edgeRadius * 2,
    speedNobeTop = this.defaultProperty.edgeRadius - this.defaultProperty.speedNobeH / 2,
    speedNobeAngle = this.defaultProperty.initDeg,
    speedPositionTxtTL = this.defaultProperty.edgeRadius - this.defaultProperty.speedPositionTxtWH / 2,
    tempDiv = '',
    induCatorLinesPosY, induCatorLinesPosX, induCatorNumbPosY, induCatorNumbPosX,
    induCatorLinesPosLeft, induCatorLinesPosTop, induCatorNumbPosLeft, induCatorNumbPosTop;

  this.setCssProperty = function () {
    var tempStyleVar = [
      '<style>',
        '#' + this.parentElemId + ' .envelope{',
          'width  :' + speedoWH + 'px;',
          'height :' + speedoWH + 'px;',
        '}',
        '#' + this.parentElemId + ' .speedNobe{',
          'height            :' + this.defaultProperty.speedNobeH + 'px;',
          'top               :' + speedNobeTop + 'px;',
          'transform         :rotate(' + speedNobeAngle + 'deg);',
          '-webkit-transform :rotate(' + speedNobeAngle + 'deg);',
          '-moz-transform    :rotate(' + speedNobeAngle + 'deg);',
          '-o-transform      :rotate(' + speedNobeAngle + 'deg);',
        '}',
        '#' + this.parentElemId + ' .speedPosition{',
          'width  :' + this.defaultProperty.speedPositionTxtWH + 'px;',
          'height :' + this.defaultProperty.speedPositionTxtWH + 'px;',
          'top  :' + speedPositionTxtTL + 'px;',
          'left :' + speedPositionTxtTL + 'px;',
        '}',
        '#' + this.parentElemId + ' .speedNobe div{',
          'width  :' + this.defaultProperty.speedoNobeW + 'px;',
          'left :' + this.defaultProperty.speedoNobeL + 'px;',
        '}',
        '#' + this.parentElemId + ' .numb{',
          'width  :' + this.defaultProperty.numbW + 'px;',
          'height :' + this.defaultProperty.numbH + 'px;',
        '}',
      '</style>',
    ].join('');
    this.parentElem.append(tempStyleVar);
  }

  this.createHtmlElements = function () {
    this.parentElemId = 'speedometerWraper-' + $(object).attr('id');
    $(object).wrap('<div id="' + this.parentElemId + '">');
    this.parentElem = $(object).parent();
    this.setCssProperty();
    this.createIndicators();
  }

  this.createIndicators = function () {
    this.parentElem.append('<div class="envelope"><svg id="speed-circ-1" viewBox="0 0 100 100"><circle cx="50" cy="50" r="50" fill="transparent" /></svg><div id="speed-cont" data-pct="100"><svg id="speed-circ-2" viewBox="0 0 100 100"><circle id="bar" r="49.5" cx="50" cy="50" fill="transparent" /></svg></div><div id="fuel-cont" data-pct="100"><svg id="fuel-circ-2" viewBox="0 0 100 100"><circle id="bar" r="49.5" cx="50" cy="50" fill="transparent" /></svg></div>');

    var speedNobe = '<div class="speedNobe"><div></div></div><div class="speedPosition"><span id="speed">0</span><span style="color: #9395a1;font-size: 12px;">km/h</span></div>';

    var fuel = '<div class="fuel"><img src="assets/images/icons/gas-station.svg" style="bottom: 16px;left: 66px;" />' +
      '<span style="bottom: 9px;left: 97px;">1/2</span><span style="bottom: 14px;left: 134px;">1</span>' +
      '<div class="strip" style="bottom: 5px;left: 70px;"></div><div class="strip" style="bottom: 5px;left: 70px;"></div><div class="strip" style="bottom: -0.25px;left: 103px;"></div><div class="strip" style="bottom: 5px;left: 136px;"></div>' +
      '<svg id="fuel-circ-1" viewBox="0 0 100 100"><circle cx="50" cy="50" r="49.75" fill="transparent" /></svg></div>'

    this.parentElem.find(".envelope").append(speedNobe + fuel);
    
    this.createSpeedCircle();
  }
  
  this.createSpeedCircle = function () {
    for (var i = 0; i <= noOfDev; i++) {
      var curDig = this.defaultProperty.initDeg + i * divDeg;
      var curIndVal = i * this.defaultProperty.divFact;
      var dangCls = "";
      if (curIndVal >= this.defaultProperty.dangerLevel) {
        dangCls = "danger";
      }
      var induCatorLinesPosY = this.defaultProperty.indicatorRadius * Math.cos(0.01746 * curDig);
      var induCatorLinesPosX = this.defaultProperty.indicatorRadius * Math.sin(0.01746 * curDig);

      var induCatorNumbPosY = this.defaultProperty.indicatorNumbRadius * Math.cos(0.01746 * curDig);
      var induCatorNumbPosX = this.defaultProperty.indicatorNumbRadius * Math.sin(0.01746 * curDig);

      if (i % this.defaultProperty.noOfSmallDiv == 0) {
        induCatorLinesPosLeft = (this.defaultProperty.edgeRadius - induCatorLinesPosX) - 2;
        induCatorLinesPosTop = (this.defaultProperty.edgeRadius - induCatorLinesPosY) - 10;
        var tempDegInd = [
                  'transform         :rotate(' + curDig + 'deg)',
                  '-webkit-transform :rotate(' + curDig + 'deg)',
                  '-o-transform      :rotate(' + curDig + 'deg)',
                  '-moz-transform    :rotate(' + curDig + 'deg)',
                ].join(";");
        induCatorNumbPosLeft = (this.defaultProperty.edgeRadius - induCatorNumbPosX) - (this.defaultProperty.numbH / 2);
        induCatorNumbPosTop = (this.defaultProperty.edgeRadius - induCatorNumbPosY) - (this.defaultProperty.numbW / 2);
        tempDiv += '<div class="numb ' + dangCls + '" style="text-align: center;left:' + induCatorNumbPosTop + 'px;top:' + induCatorNumbPosLeft + 'px;">' + curIndVal + '</div>';
      }
    }
    
    this.parentElem.find(".envelope").append(tempDiv);
    
    tempDiv = '';
  }
  
  this.removeSpeedCircle = function () {
    this.parentElem.find(".envelope").find('.numb').remove();
  }

  this.changeSpeed = function (value) {
    if (value > self.defaultProperty.maxSpeed) {
      value = self.defaultProperty.maxSpeed;
    } else if (value < 0 || isNaN(value)) {
      value = 0;
    }

    var radius = $('#speed-circ-2 #bar').attr('r');
    var circumference = 2 * Math.PI * radius;
    
    var factor = ((self.defaultProperty.maxDeg / 360) * 100 / self.defaultProperty.maxSpeed);

    var percent = ((100 - value * factor) / 100) * circumference;

    $('#speed-circ-2 #bar').css({
      strokeDashoffset: percent
    });

    $('#speed-cont').attr('data-pct', value);

    speedInDeg = (self.defaultProperty.maxDeg / self.defaultProperty.maxSpeed) * value + self.defaultProperty.initDeg;

    if (value >= self.defaultProperty.dangerLevel) {
      self.parentElem.find(".speedNobe div").addClass("danger");
      self.parentElem.find("#speed-circ-1").addClass("danger");
      self.parentElem.find("#speed-circ-2").addClass("danger");
    } else {
      self.parentElem.find(".speedNobe div").removeClass("danger");
      self.parentElem.find("#speed-circ-1").removeClass("danger");
      self.parentElem.find("#speed-circ-2").removeClass("danger");
    }

    self.parentElem.find(".speedNobe").css({
      "-webkit-transform": 'rotate(' + speedInDeg + 'deg)',
      "-webkit-transform": 'rotate(' + speedInDeg + 'deg)',
      "-moz-transform": 'rotate(' + speedInDeg + 'deg)',
      "-o-transform": 'rotate(' + speedInDeg + 'deg)'
    });
    self.parentElem.find(".speedPosition").find("#speed").html(value);
  }

  this.changeFuel = function (value) {
    if (value > 100) {
      value = 100;
    } else if (value < 0 || isNaN(value)) {
      value = 0;
    }

    var radius = $('#fuel-circ-2 #bar').attr('r');
    var circumference = 2 * Math.PI * r;

    var percent = ((100 + value * 0.1045) / 100) * c;

    $('#fuel-circ-2 #bar').css({
      strokeDashoffset: percent
    });

    $('#fuel-cont').attr('data-pct', value);

    if (value < 50) {
      self.parentElem.find("#fuel-circ-1").addClass("danger");
      self.parentElem.find("#fuel-circ-2").addClass("danger");
    } else {
      self.parentElem.find("#fuel-circ-1").removeClass("danger");
      self.parentElem.find("#fuel-circ-2").removeClass("danger");
    }
  }
  
  this.changeMaxSpeed = function (value) {
    self.defaultProperty.maxSpeed = value;
    self.defaultProperty.dangerLevel = Math.round(value / 1.35);
    self.defaultProperty.noOfSmallDiv = Math.round(value / 75);
    
    noOfDev = this.defaultProperty.maxSpeed / this.defaultProperty.divFact;
    divDeg = this.defaultProperty.maxDeg / noOfDev;
    
    this.removeSpeedCircle();
    this.createSpeedCircle();
  }

  this.createHtmlElements();
}
