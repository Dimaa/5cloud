var menu = new Menu();

function loadMenu() {
  menu.createCategory('body');
  this.loadBodyCategory();

  menu.createCategory('head');
  this.loadHeadCategory();

  menu.showMenu(1500, 3000);
}

function loadBodyCategory() {
  /** PARENTS **/

  menu.createSelection('Vater', 'fathers', '.body .parents .person');
  menu.createSelection('Mutter', 'mothers', '.body .parents .person');

  menu.createSlider('resemblance', 'Ähnlichkeit', '.body .parents .percent');
  menu.createSlider('skin-tone', 'Hautfarbe', '.body .parents .percent');

  /** PARENTS END **/
}

function loadHeadCategory() {
  /** SUB-CATEGORIES **/

  menu.createSubCategory('head', 'head');
  menu.createSubCategory('hair', 'head');
  menu.createSubCategory('eyes', 'head');
  menu.createSubCategory('cosmetic', 'head');

  /** SUB-CATEGORIES END **/

  /** HEAD **/

  for (headForm in headForms) {
    var name = headForms[headForm];
    menu.createSlider(name, name, '.head .content .head .head-form');
  }

  /** HEAD END **/

  /** HAIR **/

  menu.createImagePalette('male-hairstyle', 'Frisur', hairIDs[0], 'hairstyles/male', '.head .content .hair .hairstyle .male');
  menu.createImagePalette('female-hairstyle', 'Frisur', hairIDs[1], 'hairstyles/female', '.head .content .hair .hairstyle .female');
  menu.createColorPalette('haircolor', 'Haarfarbe', hairColors, '.head .content .hair', 1);
  menu.createColorPalette('highlighthaircolor', 'Highlightfarbe', hairColors, '.head .content .hair', 0);

  /** HAIR END **/

  /** EYES **/

  menu.createImagePalette('eyecolor', 'Augenfarbe', eyeColorIDs, 'eyecolors', '.head .content .eyes');
  menu.createImagePalette('eyebrows', 'Augenbrauen', eyeBrowesIDs, 'eyebrowes', '.head .content .eyes');
  menu.createColorPalette('eyebrowscolor', 'Augenbrauenfarbe', hairColors, '.head .content .eyes', 0);

  /** EYES END **/

  /** COSMETIC **/

  for (component in headComponents) {
    var index = headComponents[component][0];
    var name = headComponents[component][1];
    var list = 'headComponentNames[' + index + ']';
    var colors = headComponents[component][2];

    menu.createSelection(name, list, '.head .content .cosmetic .cosmetic-form');
    menu.createSlider(name, 'Sichtbarkeit', '.head .content .cosmetic .cosmetic-form', true);

    if (colors !== null) {
      menu.createColorPalette(name, 'Farbe', colors, '.head .content .cosmetic .cosmetic-form', 2);
    }

    $('.head .content .cosmetic .cosmetic-form').append('<hr />');
  }

  /** COSMETIC END **/
}

$(document).ready(function () {
  loadMenu();

  $('ul.icon-menu li').click(function () {
    var name = $(this).attr('id');
    menu.switchCategory(name);
  });

  $('ul.category li').click(function () {
    var name = $(this).attr('id');
    var category = $(this).parent().parent().attr('class');
    menu.switchSubCategory(name, category);
  });

  $('ul.category li').click(function () {
    var name = $(this).attr('id');
    var category = $(this).parent().parent().attr('class');
    menu.switchSubCategory(name, category);
  });

  $('.gender button').click(function () {
    var gender = $(this).attr('name');
    menu.setGender(gender);
  });

  $('.switcher .arrow').click(function () {
    var name = $(this).parent().attr('id');
    var direction = $(this).attr('class').split(' ')[1];
    menu.changeSelectionDirection(name, direction);
  });

  $('.color-field div').click(function () {
    var name = $(this).parent().parent().attr('id');
    var id = $(this).attr('id');
    menu.selectColorContainer(name, id);
  });

  $('.image-field div').click(function () {
    var name = $(this).parent().parent().attr('id');
    var id = $(this).attr('id');
    menu.selectImageContainer(name, id);
  });

  $('input[type=range]').on('input', function () {
    var name = $(this).attr('name');
    var value = $(this).val();
    resourceCall('changeRangeValue', name, value);
  });

  $('.create button').click(function () {
    menu.askCreateCharacter();
  })
});
