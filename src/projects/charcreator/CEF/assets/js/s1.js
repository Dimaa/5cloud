var Menu = function () {
  var categories = new Array();
  var subCategories = new Array();

  var selections = new Array();

  var colorPalettes = new Array();
  var imagePalettes = new Array();

  var currentGender = 'male';

  this.showMenu = function (duration1, duration2) {
    $('.icon-menu').fadeIn(duration1);
    $('.customize-menu').fadeIn(duration2);
    $('.rotation').fadeIn(duration2);
  }

  /** CATEGORIES **/

  this.createCategory = function (name) {
    if (!this.existCategory(name)) {
      var category = new Array();
      category.push(name);
      category.push(false);

      categories.push(category);
      subCategories.push(new Array());

      $('.icon-menu').append(String.format('<li id="{0}"><img src="assets/images/icons/{0}.svg"></li><hr />', name));

      if (categories.length <= 1) {
        this.setCategoryStatus(name, true);
      }
    }
  }

  this.switchCategory = function (name) {
    if (this.existCategory(name)) {
      var activeCategoryIndex = categories.findIndex(c => c[1] == true);
      var activeCategoryName = this.getCategoryName(activeCategoryIndex);

      if (name !== activeCategoryName) {
        /*$('.customize-menu>.content').find(String.format('.{0}', activeCategoryName)).fadeOut(500).promise().done(function () {
          $('.customize-menu>.content').find(String.format('.{0}', name)).fadeIn(500);
        });*/

        $('.customize-menu>.content').find(String.format('.{0}', activeCategoryName)).hide();
        $('.customize-menu>.content').find(String.format('.{0}', name)).show();


        this.setCategoryStatus(name, true);
        this.setCategoryStatus(activeCategoryName, false);
      }
    }
  }

  this.existCategory = function (name) {
    return (this.getCategoryIndex(name) > -1);
  }

  this.getCategoryIndex = function (name) {
    return categories.findIndex(c => c[0] == name);
  }

  this.getCategoryName = function (index) {
    return categories[index][0];
  }

  this.setCategoryStatus = function (name, status) {
    if (this.existCategory(name)) {
      var index = this.getCategoryIndex(name);
      categories[index][1] = status;
    }
  }

  /** CATEGORIES END **/

  /** SUB-CATEGORIES **/

  this.createSubCategory = function (name, category) {
    if (!this.existSubCategory(name, category)) {
      var subCategory = new Array();
      subCategory.push(name);
      subCategory.push(false);

      subCategories[this.getCategoryIndex(category)].push(subCategory);

      $('.customize-menu>.content').find(String.format('.{0}', category)).find('ul.category').append(String.format('<li id="{0}"><img src="assets/images/icons/{0}.svg"></li><hr />', name));

      if (subCategories[this.getCategoryIndex(category)].length <= 1) {
        this.setSubCategoryStatus(name, category, true);
      }
    }
  }

  this.switchSubCategory = function (name, category) {
    if (this.existSubCategory(name, category)) {
      var categoryIndex = this.getCategoryIndex(category);
      var subCategoryIndex = this.getSubCategoryIndex(name, category);
      var activeSubCategoryIndex = subCategories[categoryIndex].findIndex(c => c[1] == true);
      var activeSubCategoryName = subCategories[categoryIndex][activeSubCategoryIndex][0];

      if (name !== activeSubCategoryName) {
        /*$('.customize-menu>.content').find(String.format('.{0}', category)).find('.content').find(String.format('.{0}', activeSubCategoryName)).fadeOut(500).promise().done(function () {
          $('.customize-menu>.content').find(String.format('.{0}', category)).find('.content').find(String.format('.{0}', name)).fadeIn(500);
        });*/

        $('.customize-menu>.content').find(String.format('.{0}', category)).find('.content').find(String.format('.{0}', activeSubCategoryName)).hide();
        $('.customize-menu>.content').find(String.format('.{0}', category)).find('.content').find(String.format('.{0}', name)).show();

        this.setSubCategoryStatus(name, category, true);
        this.setSubCategoryStatus(activeSubCategoryName, category, false);
      }
    }
  }

  this.existSubCategory = function (name, category) {
    if (this.existCategory(category)) {
      var categoryIndex = this.getCategoryIndex(category);
      var subCategoryIndex = this.getSubCategoryIndex(name, category);

      return (subCategoryIndex > -1);
    }
    return false;
  }

  this.getSubCategoryIndex = function (name, category) {
    var categoryIndex = this.getCategoryIndex(category);
    var subCategoryIndex = subCategories[categoryIndex].findIndex(c => c[0] == name);
    return subCategoryIndex;
  }

  this.setSubCategoryStatus = function (name, category, status) {
    if (this.existSubCategory(name, category)) {
      var categoryIndex = this.getCategoryIndex(category);
      var subCategoryIndex = this.getSubCategoryIndex(name, category);
      subCategories[categoryIndex][subCategoryIndex][1] = status;
    }
  }

  /** SUB-CATEGORIES END **/

  /** SLIDER **/

  this.createSlider = function (name, title, object) {
    if (title !== null) {
      $(object).append(String.format('<div class="slider"><p>{0}</p><input class="male" type="range" name="{1}"></div>', title, name.toLowerCase()));
    } else {
      $(object).append(String.format('<div class="slider"><input class="male" type="range" name="{0}"></div>', name.toLowerCase()));
    }
  }

  /** SLIDER END **/

  /** COLOR-PALETTE **/

  this.createColorPalette = function (name, title, colors, object, type) {
    var colorPalette = new Array();

    if (type == 0 || type == 1) {
      if (title !== null) {
        $(object).append(String.format('<p id="title">{0}</p>', title));
      }
      $(object).append(String.format('<div class="color-palette-{0}" id="{1}"></div>', type, name.toLowerCase()));
    } else if (type == 2) {
      $(object).append(String.format('<div style="display:inline-flex;width:95%;align-items: flex-end;"><p style="width:50%;">{0}</p><div class="color-palette-{1}" id="{2}"></div></div>', title, type, name.toLowerCase()));
    }


    var primaryColors = new Array();
    var secondaryColors = new Array();

    for (color in colors) {
      if (!Array.isArray(colors[color])) {
        var primaryColor = new Array();
        var id = color;
        var hex = colors[color];
        primaryColor.push(id);
        primaryColor.push(hex);

        primaryColors.push(primaryColor);
      } else {
        var secondaryColor = new Array();
        var id = color;
        var primaryHex = colors[color][0];
        var secondaryHex = colors[color][1];
        secondaryColor.push(id);
        secondaryColor.push(primaryHex);
        secondaryColor.push(secondaryHex);

        secondaryColors.push(secondaryColor);
      }
    }

    colorPalette.push(name.toLowerCase());
    colorPalette.push(title);
    colorPalette.push(primaryColors);
    colorPalette.push(secondaryColors);
    colorPalette.push(0);
    colorPalette.push(type);

    colorPalettes.push(colorPalette);

    switch (type) {
      case 0:
        for (color in primaryColors) {
          $(object).find(String.format('.color-palette-0#{0}', name.toLowerCase())).append(String.format('<div class="color-field"><div class="color1" id="{0}" style="background-color: #{1}"></div></div>', primaryColors[color][0], primaryColors[color][1]));
        }
        break;
      case 1:
        for (color in primaryColors) {
          $(object).find(String.format('.color-palette-1#{0}', name.toLowerCase())).append(String.format('<div class="color-field"><div class="color1" id="{0}" style="background-color: #{1}"></div></div>', primaryColors[color][0], primaryColors[color][1]));
        }
        for (color in secondaryColors) {
          $(object).find(String.format('.color-palette-1#{0}', name.toLowerCase())).append(String.format('<div class="color-field"><div class="color2" id="{0}" style="background: linear-gradient(-45deg, #{2} 0%, #{2} 50%, #{1} 50%, #{1} 50%);"></div></div>', secondaryColors[color][0], secondaryColors[color][1], secondaryColors[color][2]));
        }
        break;
      case 2:
        for (color in colors) {
          $(object).find(String.format('.color-palette-2#{0}', name.toLowerCase())).append(String.format('<div class="color-field"><div class="color1" id="{0}" style="background-color: #{1}"></div></div>', color, colors[color]));
        }
        break;
    }

    $(object).find(String.format('.color-palette-{0}#{1} .color-field .color1[id="0"]', type, name.toLowerCase())).addClass('selected');
  }

  this.selectColorContainer = function (name, id) {
    var paletteIndex = colorPalettes.findIndex(c => c[0] == name);

    if (paletteIndex > -1) {
      var paletteType = colorPalettes[paletteIndex][5];

      $(String.format('.color-palette-{0}#{1}', paletteType, name)).find(String.format('.color-field div#{0}', colorPalettes[paletteIndex][4])).removeClass('selected');
      $(String.format('.color-palette-{0}#{1}', paletteType, name)).find(String.format('.color-field div#{0}', id)).addClass('selected');

      colorPalettes[paletteIndex][4] = id;
      resourceCall('changeColorValue', name, id);
    }
  }

  /** COLOR-PALETTE END **/

  /** IMAGE-PALETTE **/

  this.createImagePalette = function (name, title, ids, path, object) {
    var imagePalette = new Array();
    imagePalette.push(name);
    imagePalette.push(title);
    imagePalette.push(ids);
    imagePalette.push(path);
    imagePalette.push(ids[0]);

    imagePalettes.push(imagePalette);

    if (title !== null) {
      $(object).append(String.format('<p id="title">{0}</p>', title));
    }
    $(object).append(String.format('<div class="image-palette" id="{0}"></div>', name.toLowerCase()));

    for (id in ids) {
      $(object).find(String.format('.image-palette#{0}', name.toLowerCase())).append(String.format('<div class="image-field"><div class="container" id="{0}" style="background-image: url(assets/images/{1}/{0}.png)"></div>', ids[id], path));
    }

    $(object).find(String.format('.image-palette#{0} .image-field .container[id="{1}"]', name.toLowerCase(), ids[0])).addClass('selected');
  }

  this.selectImageContainer = function (name, id) {
    var paletteIndex = imagePalettes.findIndex(c => c[0] == name);

    if (paletteIndex > -1) {
      $(String.format('#{0} .image-field div#{1}', name, imagePalettes[paletteIndex][4])).removeClass('selected');
      $(String.format('#{0} .image-field div#{1}', name, id)).addClass('selected');

      imagePalettes[paletteIndex][4] = id;
      resourceCall('changeImageValue', name, id);
    }
  }

  /** IMAGE-PALETTE END **/

  /** SELECTION **/

  this.createSelection = function (name, listname, object) {
    var selection = new Array();
    selection.push(name);
    selection.push(listname);
    selection.push(object);
    selection.push(0);

    selections.push(selection);

    var list = eval(listname);

    $(object).append(String.format('<p>{0}</p><div class="switcher" id="{1}"><div class="arrow left a{3}">&lt;</div><div class="field {3}">{2}</div><div class="arrow right a{3}">&gt;</div></div>', name, name.toLowerCase(), list[0], currentGender));
  }

  this.changeSelectionDirection = function (name, direction) {
    var index = selections.findIndex(s => s[0].toLowerCase() == name);
    var listname = selections[index][1];
    var listindex = selections[index][3];

    switch (direction) {
      case 'left':
        if (listindex - 1 <= -1) {
          selections[index][3] = eval(listname).length - 1;
        } else {
          selections[index][3]--;
        }
        break;
      case 'right':
        if (listindex + 1 >= eval(listname).length) {
          selections[index][3] = 0;
        } else {
          selections[index][3]++;
        }
        break;
    }

    $(selections[index][2]).find(String.format('.switcher#{0}', name)).find('.field').text(eval(listname)[selections[index][3]]);
    resourceCall('changeSelectionValue', name.toLowerCase(), selections[index][3]);
  }

  /** SELECTION END **/

  this.setGender = function (gender) {
    if (gender !== currentGender) {
      $('.information .gender').find(String.format('button[name="{0}"]', currentGender)).removeClass('selected');
      $('.information .gender').find(String.format('button[name="{0}"]', gender)).addClass('selected');

      $('.slider input[type=range]').removeClass(currentGender);
      $('.slider input[type=range]').addClass(gender);

      $('.create').removeClass(currentGender);
      $('.create').addClass(gender);

      $('.arrow').removeClass('a' + currentGender);
      $('.arrow').addClass('a' + gender);

      $('.switcher .field').removeClass(currentGender);
      $('.switcher .field').addClass(gender);

      $('.head .content .hair .hairstyle').find(String.format('.{0}', currentGender)).fadeOut(500).promise().done(function () {
        $('.head .content .hair .hairstyle').find(String.format('.{0}', gender)).fadeIn(500);
      });

      currentGender = gender;
      this.setDefaultValues();
      resourceCall('setGender', gender);
    }
  }

  var askedCreateCharacter = false;
  this.askCreateCharacter = function () {
    if (!askedCreateCharacter) {
      $('.create button span').fadeTo(600, 0, function () {
        $(this).text('Bist du dir sicher?');
        $(this).fadeTo(600, 1);
      });

      askedCreateCharacter = true;
    } else {
      var code = this.checkCreateCharacter();

      if (code == 0) {
        var firstname = $('.information').find('input[name="firstname"]').val();
        var lastname = $('.information').find('input[name="lastname"]').val();

        var bday = $('.information').find('input[name="day"]').val();
        var bmonth = $('.information').find('input[name="month"]').val();
        var byear = $('.information').find('input[name="year"]').val();

        $('.create button span').fadeTo(600, 0, function () {
          $(this).delay(600);
          $(this).text('Charakter wird erstellt');
          $(this).fadeTo(600, 1);
        });
        resourceCall('createCharacter', firstname, lastname, bday, bmonth, byear);
      } else {
        switch (code) {
          case 1:
            $('.create button span').fadeTo(600, 0, function () {
              $(this).delay(600);
              $(this).text('Vorname 2-12 Zeichen');
              $(this).fadeTo(600, 1);
            });
            break;
          case 2:
            $('.create button span').fadeTo(600, 0, function () {
              $(this).delay(600);
              $(this).text('Nachname 2-12 Zeichen');
              $(this).fadeTo(600, 1);
            });
            break;
          case 3:
            $('.create button span').fadeTo(600, 0, function () {
              $(this).delay(600);
              $(this).text('Geburtstag 1-31');
              $(this).fadeTo(600, 1);
            });
            break;
          case 4:
            $('.create button span').fadeTo(600, 0, function () {
              $(this).delay(600);
              $(this).text('Geburtsmonat 1-12');
              $(this).fadeTo(600, 1);
            });
            break;
          case 5:
            $('.create button span').fadeTo(600, 0, function () {
              $(this).delay(600);
              $(this).text('Geburtsjahr 1920-2001');
              $(this).fadeTo(600, 1);
            });
            break;
          case 6:
            $('.create button span').fadeTo(600, 0, function () {
              $(this).delay(600);
              $(this).text('Geburtsdatum ist falsch');
              $(this).fadeTo(600, 1);
            });
            break;
        }

        setTimeout(function () {
          $('.create button span').fadeTo(600, 0, function () {
            $(this).text('Charakter erstellen');
            $(this).fadeTo(600, 1);
          });
        }, 3000)
      }
    }
  }

  this.checkCreateCharacter = function () {
    var firstname = $('.information').find('input[name="firstname"]').val();
    var lastname = $('.information').find('input[name="lastname"]').val();

    var bday = $('.information').find('input[name="day"]').val();
    var bmonth = $('.information').find('input[name="month"]').val();
    var byear = $('.information').find('input[name="year"]').val();
    if (firstname < 2 || firstname > 12)
      return 1;
    else if (lastname < 2 || lastname > 12)
      return 2;
    else if (bday < 1 || bday > 31)
      return 3;
    else if (bmonth < 1 || bmonth > 12)
      return 4;
    else if (byear < 1920 || byear > 2001)
      return 5;
    else if (!this.isValidDate(bday, bmonth, byear))
      return 6;
    else
      return 0;
  }

  this.daysInMonth = function (month, year) {
    switch (month) {
      case 1:
        return (year % 4 == 0 && year % 100) || year % 400 == 0 ? 29 : 28;
      case 8:
      case 3:
      case 5:
      case 10:
        return 30;
      default:
        return 31
    }
  }

  this.isValidDate = function (day, month, year) {
    month = parseInt(month, 10) - 1;
    return month >= 0 && month < 12 && day > 0 && day <= this.daysInMonth(month, year);
  }

  this.setDefaultValues = function () {
    for (colorPalette in colorPalettes) {
      var paletteName = colorPalettes[colorPalette][0];

      if (paletteName !== null) {
        this.selectColorContainer(paletteName, 0);
      }
    }

    for (imagePalette in imagePalettes) {
      var paletteName = imagePalettes[imagePalette][0];

      if (paletteName !== null) {
        this.selectImageContainer(paletteName, 0);
      }
    }

    for (selection in selections) {
      var name = selections[selection][0];
      var listname = selections[selection][1];

      selections[selection][3] = 0;

      $(selections[selection][2]).find(String.format('.switcher#{0}', name.toLowerCase())).find('.field').text(eval(listname)[selections[selection][3]]);
    }

    $('input[type=range]').val(50);

    resourceCall('setDefaultValues');
  }
}
