var fathers = ['Benjamin', 'Daniel', 'Joshua', 'Noah', 'Andrew', 'Juan', 'Alex', 'Isaac', 'Evan', 'Ethan', 'Vincent', 'Angel', 'Diego', 'Adrian', 'Gabriel', 'Michael', 'Santiago', 'Kevin', 'Louis', 'Samuel', 'Anthony', 'Claude', 'Niko', 'John'];
var mothers = ['Hannah', 'Aubrey', 'Jasmine', 'Gisele', 'Amelia', 'Isabella', 'Zoe', 'Ava', 'Camila', 'Violet', 'Sophia', 'Evelyn', 'Nicole', 'Ashley', 'Gracie', 'Brianna', 'Natalie', 'Olivia', 'Elizabeth', 'Charlotte', 'Emma', 'Misty'];

var hairIDs = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
               [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38]];

var hairColors = ['1c1d1e', '272a2c', '312e2c', 
                  '35261c', '4b321f', '5c3b24', 
                  '6d4c35', '6b503b', '765c45', 
                  '7f684e', '99815d', 'a79369', 
                  'af9c70', 'bba063', 'd6b97b', 
                  'dac38e', '9f7f59', '845039', 
                  '682b1f', '940c00', 'a00b00', 
                  'b40b00', 'ba0f00', 'cc1700', 
                  'ca2800', 'd12200', '575c57',
                  '656a65', '848e84', '9ea89e',
                  '4e2f81', '6b298a', 'a60878',
                  'e700d6', 'ff0361', 'ff3977',
                  '0093a3', '0084a6', '0054a5',
                  '00ba35', '00a367', '007887',
                  '3db300', '63b200', '00a712',
                  'f5a800', 'daa100', 'd57600',
                  'd24200', 'e03200', 'e63b00',
                  'd91a00', 'c21700', 'b30000',
                  '890000', '14100c', '1a1310',
                  '1c1411', '221612', '1d1612',
                  '18130e', '0b0908', ['6a513b', 'cea878'], 
                  ['ce9e65', 'edceaa']];

var blushColors = ['992532', 'c8395d', 'bd516c',
                   'b8637a', 'a6526b', 'b1434c',
                   '7f3133', 'a4645d', 'c18779',
                   'cba096', 'c6918f', 'ab6f63',
                   'b06050', 'a84c33', 'b47178',
                   'ca7f92', 'ed9cbe', 'e775a4',
                   'de3e81', 'b34c6e', '712739',
                   '4f1f2a', 'aa222f', 'de2034',
                   'cf0813', 'e55470', 'dc3fb5',
                   'c227b2', 'a01ca9', '6e1875',
                   '731465', '56165c', '5e1e87',
                   '192d69', '163c95', '1d6daa',
                   '2a99b9', '19a9b2'];

var lipstickColors = ['992532', 'c8395d', 'bd516c',
                      'b8637a', 'a6526b', 'b1434c',
                      '7f3133', 'a4645d', 'c18779',
                      'cba096', 'c6918f', 'ab6f63',
                      'b06050', 'a84c33', 'b47178',
                      'ca7f92', 'ed9cbe', 'e775a4',
                      'de3e81', 'b34c6e', '712739',
                      '4f1f2a', 'aa222f', 'de2034',
                      'cf0813', 'e55470', 'dc3fb5',
                      'c227b2', 'a01ca9', '6e1875',
                      '731465', '56165c', '5e1e87',
                      '192d69', '163c95', '1d6daa',
                      '2a99b9', '19a9b2'];

var eyeBrowesIDs = [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33];
var eyeColorIDs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31];

var headForms = ['Nasenbreite', 'Nasenhöhe', 'Nasenspitzenlänge',
                 'Nasenrückentiefe', 'Nasenspitzenhöhe', 'Nasenbruch',
                 'Augenbrauenhöhe', 'Augenbrauentiefe', 'Jochbeinhöhe',
                 'Jochbeinbreite', 'Wangentiefe', 'Augengröße',
                 'Lippendicke', 'Kieferbreite', 'Kieferform',
                 'Kinnhöhe', 'Kinnbreite', 'Kinngrübchen',
                 'Nackenbreite'];

var headComponents = [[0, 'Hautunreinheiten', null],
                      [1, 'Gesichtsbehaarung', null],
                      [3, 'Hautalterung', null],
                      [4, 'Augen-Make-up', null],
                      [5, 'Rouge', blushColors],
                      [6, 'Teint', null],
                      [7, 'Hautschaden', null],
                      [8, 'Lippenstift', lipstickColors],
                      [9, 'Hautmale', null],
                      //[10, 'Brusthaare', 0, 16],
                      //[11, 'Körperunreinheiten', 0, 11]
                     ];

var headComponentNames = [ // Hautunreinheiten [0]
                          ['Keine', 'Masern', 'Pickel',
                           'Hautunreinheiten', 'Ausschlag', 'Mitesser',
                           'Erhitzt', 'Pusteln', 'Eiterpusteln',
                           'Furunkulose', 'Akne', 'Ringelröteln',
                           'Gesichtsausschlag', 'Nasenbohrer', 'Pubertät',
                           'Matschauge', 'Kinnausschlag', 'Two Face',
                           'T-Zone', 'Fettig', 'Narbig',
                           'Aknenarben', 'Totale Katerlandschaft', 'Herpes',
                           'Eiterflechte'],
                          // Gesichtsbehaarung [1]
                          ['Glatt rasiert', 'Kurze Stoppeln', 'Balbo',
                           'Henriquatre', 'Spitzbart', 'Kinnbart',
                           'Kinnflaum', 'Dünner Kinnstreifen',
                           'Ungepflegt', 'Knebelbart', 'Schnurrbart',
                           'Kurzer Bart', 'Dreitagebart', 'Dünner Henriquatre',
                           'Mongolenbart', 'Stift und Koteletten', 'Kinnstreifen',
                           'Balbo und Backenbart', 'Koteletten', 'Ungepflegter Bart',
                           'Gezwirbelt', 'Gezwirbelt & Dreitagebart', 'Langer Schnäuzer',
                           'Faustisch', 'Otto & Kinnbart', 'Otto & Vollbart',
                           'Dünner Franz', 'Schnäuzer & Koteletten', 'Backenbart',
                           'Lincoln-Kinnbart'],
                          // Augenbrauen [2]
                          [],
                          // Hautalterung [3]
                          ['Keine', 'Krähenfüße', 'Erste Anzeichen', 'Mittelalt',
                           'Sorgenfalten', 'Depression', 'Distinguiert',
                           'Gealtert', 'Verwittert', 'Faltig',
                           'Schlaff', 'Verlebt', 'Oldie',
                           'Ruhestand', 'Junkie', 'Geriatrix'],
                          // Augen-Make-up [4]
                          ['Keine', 'Verruchtes Schwarz', 'Bronze',
                           'Hellgrau', 'Retro-Glam', 'Natürlicher Look',
                           'Cat Eyes', 'Chola', 'Vamp',
                           'Vinewood-Glamour', 'Bubblegum', 'Aqua-Traum',
                           'Pin-up', 'Lila Leidenschaft', 'Verruchtes Cat Eye',
                           'Glühender Rubin', 'Pop-Prinzessin'],
                          // Rouge [5]
                          ['Keine', 'Voll', 'Schräg',
                           'Rund', 'Horizontal', 'Hoch',
                           'Schätzchen', 'Achtziger'],
                          // Teint [6]
                          ['Keine', 'Rotbäckchen', 'Stoppelausschlag',
                           'Hitzewallung', 'Sonnenbrand', 'Blutunterlaufen',
                           'Alkoholiker', 'Fleckig', 'Totem',
                           'Äderchen', 'Lädiert', 'Bleich',
                           'Gespenstisch'],
                          // Hautschaden [7]
                          ['Keine', 'Uneben', 'Sandpapier',
                           'Ungleichmäßig', 'Rau', 'Ledrig',
                           'Strukturiert', 'Grob', 'Zerklüftet',
                           'Knittrig', 'Rissig', 'Hart'],
                          // Lippenstift [8]
                          ['Keine', 'Farbe, matt', 'Farbe, Glanz',
                           'Umrandet, matt', 'Unrandet, Glanz', 'Stark umrandet, matt',
                           'Stark umrandet, Glanz', 'Nude Look, umrandet, matt', 'Nude Look, umrandet, Glanz',
                           'Verwischt', 'Geisha'],
                          // Hautmale [9]
                          ['Keine', 'Engelchen', 'Überall',
                           'Unregelmäßig', 'Fleckentanz', 'Über die Brücke',
                           'Babypuppe', 'Kobold', 'Sonnenverwöhnt',
                           'Schönheitsflecken', 'Reihenweise', 'Fotomodelling',
                           'Vereinzelt', 'Gesprenkelt', 'Regentropfen',
                           'Beide Bäckchen', 'Einseitig', 'Paarweise',
                           'Wucher'],
                          // Brusthaare [10]
                          [],
                          // Körperunreinheiten [11]
                          []
];
