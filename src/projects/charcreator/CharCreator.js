"use strict";
/// <reference path="../../types-gt-mp/index.d.ts" />
var player = API.getLocalPlayer();
var browser = null;
API.onServerEventTrigger.connect(function (eventName, args) {
    switch (eventName) {
        case 'openCharCreator':
            openCharCreator();
            break;
        case 'closeCharCreator':
            closeCharCreator();
            break;
        case 'destroyCharCreator':
            destroyCharCreator();
            break;
    }
});
function createCharCreator() {
    var resolution = API.getScreenResolution();
    browser = API.createCefBrowser(resolution.Width, resolution.Height);
    API.setCefBrowserPosition(browser, 0, 0);
    API.setCefBrowserHeadless(browser, true);
    API.waitUntilCefBrowserInit(browser);
    API.loadPageCefBrowser(browser, 'projects/charcreator/CEF/index.html');
}
function destroyCharCreator() {
    if (browser !== null) {
        closeCharCreator();
        API.destroyCefBrowser(browser);
    }
}
function openCharCreator() {
    if (browser !== null) {
        var charCamera = API.createCamera(new Vector3(402.8664, -997.5515, -98.5), new Vector3(0, 0, 0));
        API.pointCameraAtPosition(charCamera, new Vector3(402.8664, -996.4108, -98.5));
        API.setActiveCamera(charCamera);
        API.setCefBrowserHeadless(browser, false);
        API.showCursor(true);
        API.setCanOpenChat(false);
        API.setChatVisible(false);
        API.setHudVisible(false);
        API.setPlayerSkin(1885233650);
        setDefaultValues();
    }
    else {
        createCharCreator();
        openCharCreator();
    }
}
function closeCharCreator() {
    if (browser !== null) {
        API.setActiveCamera(null);
        API.setCefBrowserHeadless(browser, true);
        API.showCursor(false);
        API.setCanOpenChat(true);
        API.setChatVisible(true);
        API.setHudVisible(true);
    }
}
var currentGender = 'male';
var currentMother;
var currentFather;
var currentResemblance;
var currentSkintone;
var currentHairStyle;
var currentHairColor;
var currentHighlightHairColor;
var currentNoseWidth;
var currentNoseBottomHeight;
var currentNoseTipLength;
var currentNoseBridgeDepth;
var currentNoseTipHeight;
var currentNoseBroken;
var currentBrowHeight;
var currentBrowDepth;
var currentCheekboneHeight;
var currentCheekboneWidth;
var currentCheekDepth;
var currentEyeSize;
var currentLipThickness;
var currentJawWidth;
var currentJawShape;
var currentChinHeight;
var currentChinDepth;
var currentChinWidth;
var currentChinIndent;
var currentNeckWidth;
var currentEyeColor;
var currentEyebrows;
var currentEyebrowsColor;
var currentBlemishes;
var currentBlemishesOpacity;
var currentFacialHair;
var currentFacialHairOpacity;
//var currentFacialHairColor: number;
var currentAgeing;
var currentAgeingOpacity;
var currentEyeMakeup;
var currentEyeMakeupOpacity;
var currentBlush;
var currentBlushOpacity;
var currentBlushColor;
var currentComplexion;
var currentComplexionOpacity;
var currentSunDamage;
var currentSunDamageOpacity;
var currentLipstick;
var currentLipstickOpacity;
var currentLipstickColor;
var currentMoles;
var currentMolesOpacity;
function setDefaultValues() {
    currentMother = 0;
    currentFather = 0;
    currentResemblance = 50;
    currentSkintone = 50;
    currentHairStyle = 0;
    currentHairColor = 0;
    currentHighlightHairColor = 0;
    currentNoseWidth = 50;
    currentNoseBottomHeight = 50;
    currentNoseTipLength = 50;
    currentNoseBridgeDepth = 50;
    currentNoseTipHeight = 50;
    currentNoseBroken = 50;
    currentBrowHeight = 50;
    currentBrowDepth = 50;
    currentCheekboneHeight = 50;
    currentCheekboneWidth = 50;
    currentCheekDepth = 50;
    currentEyeSize = 50;
    currentLipThickness = 50;
    currentJawWidth = 50;
    currentJawShape = 50;
    currentChinHeight = 50;
    currentChinDepth = 50;
    currentChinWidth = 50;
    currentChinIndent = 50;
    currentNeckWidth = 50;
    currentEyeColor = 0;
    currentEyebrows = -1;
    currentEyebrowsColor = 0;
    currentBlemishes = -1;
    currentBlemishesOpacity = 50;
    currentFacialHair = -1;
    currentFacialHairOpacity = 50;
    //currentFacialHairColor = 0;
    currentAgeing = -1;
    currentAgeingOpacity = 50;
    currentEyeMakeup = -1;
    currentEyeMakeupOpacity = 50;
    currentBlush = -1;
    currentBlushOpacity = 50;
    currentBlushColor = 0;
    currentComplexion = -1;
    currentComplexionOpacity = 50;
    currentSunDamage = -1;
    currentSunDamageOpacity = 50;
    currentLipstick = -1;
    currentLipstickOpacity = 50;
    currentLipstickColor = 0;
    currentMoles = -1;
    currentMolesOpacity = 50;
    updateCharacterParents();
    updateHair();
    updateEyebrows();
    updateEyes();
    updateColors();
}
function setGender(gender) {
    switch (gender) {
        case 'male':
            API.setPlayerSkin(1885233650);
            setDefaultValues();
            currentGender = gender;
            break;
        case 'female':
            API.setPlayerSkin(-1667301416);
            setDefaultValues();
            currentGender = gender;
            break;
    }
}
function changeSelectionValue(name, index) {
    player = API.getLocalPlayer();
    switch (name) {
        case 'vater':
            currentFather = index;
            updateCharacterParents();
            break;
        case 'mutter':
            currentMother = index;
            updateCharacterParents();
            break;
        case 'hautunreinheiten':
            currentBlemishes = index - 1;
            API.setPlayerHeadOverlay(player, 0, currentBlemishes, currentBlemishesOpacity * 0.01);
            break;
        case 'gesichtsbehaarung':
            currentFacialHair = index - 1;
            API.setPlayerHeadOverlay(player, 1, currentFacialHair, currentFacialHairOpacity * 0.01);
            break;
        case 'hautalterung':
            currentAgeing = index - 1;
            API.setPlayerHeadOverlay(player, 3, currentAgeing, currentAgeingOpacity * 0.01);
            break;
        case 'augen-make-up':
            currentEyeMakeup = index - 1;
            API.setPlayerHeadOverlay(player, 4, currentEyeMakeup, currentEyeMakeupOpacity * 0.01);
            break;
        case 'rouge':
            currentBlush = index - 1;
            API.setPlayerHeadOverlay(player, 5, currentBlush, currentBlushOpacity * 0.01);
            break;
        case 'teint':
            currentComplexion = index - 1;
            API.setPlayerHeadOverlay(player, 6, currentComplexion, currentComplexionOpacity * 0.01);
            break;
        case 'hautschaden':
            currentSunDamage = index - 1;
            API.setPlayerHeadOverlay(player, 7, currentSunDamage, currentSunDamageOpacity * 0.01);
            break;
        case 'lippenstift':
            currentLipstick = index - 1;
            API.setPlayerHeadOverlay(player, 8, currentLipstick, currentLipstickOpacity * 0.01);
            break;
        case 'hautmale':
            currentMoles = index - 1;
            API.setPlayerHeadOverlay(player, 9, currentMoles, currentMolesOpacity * 0.01);
            break;
    }
}
function changeRangeValue(name, value) {
    player = API.getLocalPlayer();
    switch (name) {
        case 'rotate':
            rotateCharacter(value);
            break;
        case 'resemblance':
            currentResemblance = value;
            updateCharacterParents();
            break;
        case 'skin-tone':
            currentSkintone = value;
            updateCharacterParents();
            break;
        case 'nasenbreite':
            currentNoseWidth = value;
            API.setPlayerFaceFeature(player, 0, currentNoseWidth * 0.01);
            break;
        case 'nasenhöhe':
            currentNoseBottomHeight = value;
            API.setPlayerFaceFeature(player, 1, currentNoseBottomHeight * 0.01);
            break;
        case 'nasenspitzenlänge':
            currentNoseTipLength = value;
            API.setPlayerFaceFeature(player, 2, currentNoseTipLength * 0.01);
            break;
        case 'nasenrückentiefe':
            currentNoseBridgeDepth = value;
            API.setPlayerFaceFeature(player, 3, currentNoseBridgeDepth * 0.01);
            break;
        case 'nasenspitzenhöhe':
            currentNoseTipHeight = value;
            API.setPlayerFaceFeature(player, 4, currentNoseTipHeight * 0.01);
            break;
        case 'nasenbruch':
            currentNoseBroken = value;
            API.setPlayerFaceFeature(player, 5, currentNoseBroken * 0.01);
            break;
        case 'augenbrauenhöhe':
            currentBrowHeight = value;
            API.setPlayerFaceFeature(player, 6, currentBrowHeight * 0.01);
            break;
        case 'augenbrauentiefe':
            currentBrowDepth = value;
            API.setPlayerFaceFeature(player, 7, currentBrowDepth * 0.01);
            break;
        case 'jochbeinhöhe':
            currentCheekboneHeight = value;
            API.setPlayerFaceFeature(player, 8, currentCheekboneHeight * 0.01);
            break;
        case 'jochbeinbreite':
            currentCheekboneWidth = value;
            API.setPlayerFaceFeature(player, 9, currentCheekboneWidth * 0.01);
            break;
        case 'wangentiefe':
            currentCheekDepth = value;
            API.setPlayerFaceFeature(player, 10, currentCheekDepth * 0.01);
            break;
        case 'augengröße':
            currentEyeSize = value;
            API.setPlayerFaceFeature(player, 11, currentEyeSize * 0.01);
            break;
        case 'lippendicke':
            currentLipThickness = value;
            API.setPlayerFaceFeature(player, 12, currentLipThickness * 0.01);
            break;
        case 'kieferbreite':
            currentJawWidth = value;
            API.setPlayerFaceFeature(player, 13, currentJawWidth * 0.01);
            break;
        case 'kieferform':
            currentJawShape = value;
            API.setPlayerFaceFeature(player, 14, currentJawShape * 0.01);
            break;
        case 'kieferhöhe':
            currentChinHeight = value;
            API.setPlayerFaceFeature(player, 15, currentChinHeight * 0.01);
            break;
        case 'kinngrübchen':
            currentChinDepth = value;
            API.setPlayerFaceFeature(player, 16, currentChinDepth * 0.01);
            break;
        case 'kinnbreite':
            currentChinWidth = value;
            API.setPlayerFaceFeature(player, 17, currentChinWidth * 0.01);
            break;
        /*case 'kinngrübchen':
            currentChinIndent = value;
            API.setPlayerFaceFeature(player, 18, currentChinIndent * 0.01);
            break;*/
        case 'nackenbreite':
            currentNeckWidth = value;
            API.setPlayerFaceFeature(player, 19, currentNeckWidth * 0.01);
            break;
        case 'hautunreinheiten':
            currentBlemishesOpacity = value;
            API.setPlayerHeadOverlay(player, 0, currentBlemishes, currentBlemishesOpacity * 0.01);
            break;
        case 'gesichtsbehaarung':
            currentFacialHairOpacity = value;
            API.setPlayerHeadOverlay(player, 1, currentFacialHair, currentFacialHairOpacity * 0.01);
            break;
        case 'hautalterung':
            currentAgeingOpacity = value;
            API.setPlayerHeadOverlay(player, 3, currentAgeing, currentAgeingOpacity * 0.01);
            break;
        case 'augen-make-up':
            currentEyeMakeupOpacity = value;
            API.setPlayerHeadOverlay(player, 4, currentEyeMakeup, currentEyeMakeupOpacity * 0.01);
            break;
        case 'rouge':
            currentBlushOpacity = value;
            API.setPlayerHeadOverlay(player, 5, currentBlush, currentBlushOpacity * 0.01);
            break;
        case 'teint':
            currentComplexionOpacity = value;
            API.setPlayerHeadOverlay(player, 6, currentComplexion, currentComplexionOpacity * 0.01);
            break;
        case 'hautschaden':
            currentSunDamageOpacity = value;
            API.setPlayerHeadOverlay(player, 7, currentSunDamage, currentSunDamageOpacity * 0.01);
            break;
        case 'lippenstift':
            currentLipstickOpacity = value;
            API.setPlayerHeadOverlay(player, 8, currentLipstick, currentLipstickOpacity * 0.01);
            break;
        case 'hautmale':
            currentMolesOpacity = value;
            API.setPlayerHeadOverlay(player, 9, currentMoles, currentMolesOpacity * 0.01);
            break;
    }
}
function changeColorValue(name, index) {
    switch (name) {
        case 'haircolor':
            currentHairColor = index;
            break;
        case 'highlighthaircolor':
            currentHighlightHairColor = index;
            break;
        case 'eyebrowscolor':
            currentEyebrowsColor = index;
            break;
        case 'rouge':
            currentBlushColor = index;
            break;
        case 'lippenstift':
            currentLipstickColor = index;
            break;
    }
    updateColors();
}
function changeImageValue(name, index) {
    switch (name) {
        case 'male-hairstyle':
        case 'female-hairstyle':
            currentHairStyle = index;
            updateHair();
            break;
        case 'eyecolor':
            currentEyeColor = index;
            updateEyes();
            break;
        case 'eyebrows':
            currentEyebrows = index;
            updateEyebrows();
            break;
    }
}
function rotateCharacter(value) {
    player = API.getLocalPlayer();
    API.setEntityRotation(player, new Vector3(0.00, 0.00, parseFloat(value.toString()) * 3.6));
}
function updateCharacterParents() {
    player = API.getLocalPlayer();
    API.setPlayerHeadBlendData(player, currentMother, currentFather, 0, currentMother, currentFather, 0, currentResemblance * 0.01, currentSkintone * 0.01, 0.00, false);
}
function updateHair() {
    player = API.getLocalPlayer();
    API.setPlayerClothes(player, 2, currentHairStyle, 0);
}
function updateEyes() {
    player = API.getLocalPlayer();
    API.setPlayerEyeColor(player, currentEyeColor);
}
function updateEyebrows() {
    player = API.getLocalPlayer();
    API.setPlayerEyebrows(player, currentEyebrows, currentEyebrowsColor, 1.00);
}
function updateColors() {
    player = API.getLocalPlayer();
    API.setPlayerHairColor(player, currentHairColor, currentHighlightHairColor); // Hair
    API.setPlayerHeadOverlayColor(player, 1, 1, currentHairColor, 1.00); // Beard
    API.setPlayerHeadOverlayColor(player, 2, 1, currentEyebrowsColor, 1.00); // Eyebrows
    API.setPlayerHeadOverlayColor(player, 5, 2, currentBlushColor, 1.00); // Blush
    API.setPlayerHeadOverlayColor(player, 8, 2, currentLipstickColor, 1.00); // Lipstick
}
function createCharacter(firstname, lastname, bday, bmonth, byear) {
    firstname = firstname.charAt(0).toUpperCase() + firstname.substring(1).toLowerCase();
    lastname = lastname.charAt(0).toUpperCase() + lastname.substring(1).toLowerCase();
    bday = bday;
    bmonth = bmonth;
    byear = byear;
    API.triggerServerEvent('createCharactor', firstname, lastname, bday, bmonth, byear, currentGender, currentMother, currentFather, currentResemblance * 0.01, currentSkintone * 0.01, currentHairStyle, currentHairColor, currentHighlightHairColor, currentNoseWidth, currentNoseBottomHeight, currentNoseTipLength, currentNoseBridgeDepth, currentNoseTipHeight, currentNoseBroken, currentBrowHeight, currentBrowDepth, currentCheekboneHeight, currentCheekboneWidth, currentCheekDepth, currentEyeSize, currentLipThickness, currentJawWidth, currentJawShape, currentChinHeight, currentChinDepth, currentChinWidth, currentChinIndent, currentNeckWidth, currentEyeColor, currentEyebrows, currentEyebrowsColor, currentBlemishes, currentBlemishesOpacity, currentFacialHair, currentFacialHairOpacity, currentHairColor, currentAgeing, currentAgeingOpacity, currentEyeMakeup, currentEyeMakeupOpacity, currentBlush, currentBlushOpacity, currentBlushColor, currentComplexion, currentComplexionOpacity, currentSunDamage, currentSunDamageOpacity, currentLipstick, currentLipstickOpacity, currentLipstickColor, currentMoles, currentMolesOpacity);
}
