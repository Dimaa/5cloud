var serverTime = new Date();

function updateTime() {
  serverTime = new Date(serverTime.getTime() + 1000);
  var hours = serverTime.getHours();
  var minutes = serverTime.getMinutes();
  
  if (hours < 10) hours = "0" + hours;
  if (minutes < 10) minutes = "0" + minutes;
  
  $('span#time').html(hours + ':' + minutes);
}

$(document).ready(function () {
  updateTime();
  setInterval(updateTime, 1000);
});