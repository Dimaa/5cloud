using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;

namespace CharCreator
{
    internal class Main : Script
    {
        public Main()
        {
            API.onResourceStart += this.OnResourceStart;
            API.onClientEventTrigger += this.OnClientEventTrigger;
        }

        private void OnClientEventTrigger(Client sender, string eventName, params object[] arguments)
        {
            switch (eventName)
            {
                case "createCharactor":
                    var firstName = (string)arguments[0];
                    var lastName = (string)arguments[1];

                    var birthDay = (int)arguments[2];
                    var birthMonth = (int)arguments[3];
                    var birthYear = (int)arguments[4];

                    var gender = (string)arguments[5];

                    var mother = (int)arguments[6];
                    var father = (int)arguments[7];

                    var resemblance = (int)arguments[8];
                    var skintone = (int)arguments[9];

                    var hairStyle = (int)arguments[10];
                    var hairColor = (int)arguments[11];
                    var hairHighlightColor = (int)arguments[12];

                    var noseWidth = (int)arguments[13];
                    var noseBottomHeight = (int)arguments[14];
                    var noseTipLength = (int)arguments[15];
                    var noseBridgeDepth = (int)arguments[16];
                    var noseTipHeight = (int)arguments[17];
                    var noseBroken = (int)arguments[18];
                    var browHeight = (int)arguments[19];
                    var browDepth = (int)arguments[20];
                    var cheekboneHeight = (int)arguments[21];
                    var cheekboneWidth = (int)arguments[22];
                    var cheekDepth = (int)arguments[23];
                    var eyeSize = (int)arguments[24];
                    var lipThickness = (int)arguments[25];
                    var jawWidth = (int)arguments[26];
                    var jawShape = (int)arguments[27];
                    var chinHeight = (int)arguments[28];
                    var chinDepth = (int)arguments[29];
                    var chinWidth = (int)arguments[30];
                    var chinIndent = (int)arguments[31];
                    var neckWidth = (int)arguments[32];

                    var eyeColor = (int)arguments[33];

                    var eyebrows = (int)arguments[34];
                    var eyebrowsColor = (int)arguments[35];

                    var blemishes = (int)arguments[36];
                    var blemishesOpacity = (int)arguments[37];

                    var facialHair = (int)arguments[38];
                    var facialHairOpacity = (int)arguments[39];
                    var facialHairColor = (int)arguments[40];

                    var ageing = (int)arguments[41];
                    var ageingOpacity = (int)arguments[42];

                    var eyeMakeup = (int)arguments[43];
                    var eyeMakeupOpacity = (int)arguments[44];

                    var blush = (int)arguments[45];
                    var blushOpacity = (int)arguments[46];
                    var blushColor = (int)arguments[47];

                    var complexion = (int)arguments[48];
                    var complexionOpacity = (int)arguments[49];

                    var sunDamage = (int)arguments[50];
                    var sunDamageOpacity = (int)arguments[51];

                    var lipstick = (int)arguments[52];
                    var lipstickOpacity = (int)arguments[53];
                    var lipstickColor = (int)arguments[54];

                    var moles = (int)arguments[55];
                    var molesOpacity = (int)arguments[56];


                    API.freezePlayer(sender, false);
                    break;
            }
        }

        private void OnResourceStart()
        {

        }

        [Command("open")]
        public void OpenCharCreator(Client sender)
        {
            API.setEntityPosition(sender, new Vector3(402.8664, -996.4108, -99.00027));
            API.setEntityRotation(sender, new Vector3(0.00, 0.00, -185.00));

            API.freezePlayer(sender, true);

            API.triggerClientEvent(sender, "openCharCreator");
        }

        [Command("veh")]
        public void SpawnVehicle(Client sender)
        {
            var playerPos = API.getEntityPosition(sender);
            NetHandle vehicle = API.createVehicle(VehicleHash.Airbus, playerPos, new Vector3(0, 0, 0), 0, 0);
            var adminCarLabel = API.createTextLabel(string.Format("Admin Fahrzeug\n~b~{0}", sender.name), new Vector3(0, 0, 0), 40.0f, 0.5f);

            API.attachEntityToEntity(adminCarLabel, vehicle, null, new Vector3(0, 0, 1), new Vector3());
            API.setPlayerIntoVehicle(sender, vehicle, -1);

            API.sendPictureNotificationToPlayer(sender, string.Format("Das Fahrzeug ~b~{0} ~w~wurde gespawnt", VehicleHash.Voltic2), "CHAR_CARSITE", 1, 1, "Admin Tool", "Fahrzeug");
        }



        [Command("pos")]
        public void Position(Client sender)
        {
            var playerPos = API.getEntityPosition(sender);
            API.sendChatMessageToPlayer(sender, playerPos.ToString());
        }
    }
}